import pagin from '../pagin/pagin.vue';
export default {
    components: {
        pagin
    },
    data: function () {
        return {
            books: [],
            curentPage: 1,
            countElementScreen: 10,
            nameUser: '',
            objects: []
        }
    },
    mounted() {
        axios.get('/api/v1/books')
            .then((response) => {
                this.books = response['data']['books'];
                this.currentObject = this.books;
                this.objects = [];
                var self = this;
                for (let a1 = 0; a1 < this.countElementScreen; a1++) {
                    let keys = (self.curentPage-1) * self.countElementScreen + a1;
                    if (keys < self.books.length) {
                        self.objects.push(self.books[keys]);
                    }
                }
            })
            .catch((error) => {
            });
    },
    methods: {
        changepage(val) {

            this.curentPage = val['curentPage'];
            this.objects = [];
            if (this.curentPage == 'all') {
                this.objects = this.books;
            } else {
                var self = this;
                for (let a1 = 0; a1 < this.countElementScreen; a1++) {
                    let keys = (self.curentPage - 1) * self.countElementScreen + a1;

                    if (keys < self.books.length) {
                        self.objects.push(self.books[keys]);
                    }
                }
            }
        }
    }
}