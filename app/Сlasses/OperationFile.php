<?php
namespace App\Classes;


use mysql_xdevapi\Exception;
use App\File;

class OperationFile {

    public function saveFile($files) {//Сохраняем файл

        try {
            foreach ($files as $key => $file) {
                $destinationPath = public_path() . '/file';
                $nameFile = $file->getClientOriginalName();
                $file->move($destinationPath, $file->getClientOriginalName());
            };
            return $nameFile;
        }
        catch (Exception  $e) {
            return false;
        }

    }
    public function createFile($userId,$booksId,$data) {

        $object['file'] = empty($data['file']) ? '' : $this->saveFile([$data['file']]);

        $file = new File([
            'name' => $object['file']
        ]);

        try {
            $file->book_id = $booksId;
            $file->user_id = $userId;
            $file->save();
            return true;
        }
        catch (Exception $e) {
            return false;
        }
    }




}