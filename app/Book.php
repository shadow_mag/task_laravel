<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['name', 'subject'];

    public function files() {
        return $this->hasMany('App\File');
    }
}
