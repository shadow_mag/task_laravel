
export default {
    created: function () {
        this.pageCount = Math.floor(this.countElement/this.countElementScreen);
        if (this.countElement % this.countElementScreen > 0) {
            this.pageCount++;
        }
        if (this.pageCount<9) {
            var self =this;
            for(let a1=1;a1<=this.pageCount ;a1++) {
                self.pagins.push(a1);
            }
        }
        else {
            this.pagins = ['1','2','3','...',false,false,false,false,this.pageCount];
        }
        //console.log(this.pagins);
    },
    watch: {
        countElement: function () {
            this.pageCount = 0;
            this.pagins = [];
            this.pageCount = Math.floor(this.countElement / this.countElementScreen);
            if (this.countElement % this.countElementScreen > 0) {
                this.pageCount++;
            }
            if (this.pageCount < 9) {
                var self = this;
                for (let a1 = 1; a1 <= this.pageCount; a1++) {
                    self.pagins.push(a1);
                }
            } else {
                this.pagins = ['1', '2', '3', '...', false, false, false, false, this.pageCount];
            }
        },
        curentPage: function () {
            if (this.pageCount > 8) {
                if (this.curentPage == 1) {
                    this.pagins = ['1', '2', '3', '...', false, false, false, false, this.pageCount];
                }
                if (this.curentPage == 2) {
                    this.pagins = ['1', '2', '3', '4', '...', false, false, false, this.pageCount];
                }
                if (this.curentPage == 3) {
                    this.pagins = ['1', '2', '3', '4', '5', '...', false, false, this.pageCount];
                }
                if (this.curentPage == 4) {
                    this.pagins = ['1', '2', '3', '4', '5', '6', '...', false, this.pageCount];
                }
                if (this.curentPage > 4) {
                    this.pagins = ['1', '...', parseInt(this.curentPage) - 2, parseInt(this.curentPage) - 1, parseInt(this.curentPage), parseInt(this.curentPage) + 1, parseInt(this.curentPage) + 2, '...', parseInt(this.pageCount)];
                }
                if ((this.pageCount - this.curentPage) == 3) {
                    this.pagins = ['1', false, '...', parseInt(this.curentPage) - 2, parseInt(this.curentPage) - 1, parseInt(this.curentPage), parseInt(this.pageCount) - 2, parseInt(this.pageCount) - 1, parseInt(this.pageCount)];

                }
                if ((this.pageCount - this.curentPage) == 2) {
                    this.pagins = ['1', false, false, '...', parseInt(this.curentPage) - 2, parseInt(this.curentPage) - 1, parseInt(this.curentPage), parseInt(this.pageCount) - 1, parseInt(this.pageCount)];
                }
                if ((this.pageCount - this.curentPage) == 1) {
                    this.pagins = ['1', false, false, false, '...', parseInt(this.curentPage) - 2, parseInt(this.curentPage) - 1, parseInt(this.curentPage), parseInt(this.pageCount)];
                }
                if ((this.pageCount - this.curentPage) == 0) {
                    this.pagins = ['1', false, false, false, '...', parseInt(this.curentPage) - 3, parseInt(this.curentPage) - 2, parseInt(this.curentPage) - 1, parseInt(this.curentPage)];
                }
            }
        }
    },
    mounted() {
        axios.get('/api/v1/books')
            .then((response) => {
                console.log(response);
                this.books = response['data']['books'];
            })
            .catch((error) => {
            });
    },
    methods: {
        plusGoods() {
            if (this.curentPage != this.pageCount) {
                let requestArray = {
                    "curentPage": parseInt(this.curentPage) + 1
                };
                this.$emit('changepage', requestArray);
            }
        },
        plusMinus() {
            if (this.curentPage > 1) {
                let requestArray = {
                    "curentPage": parseInt(this.curentPage) - 1
                };
                this.$emit('changepage', requestArray);
            }
        },
        allGoods(val) {
            let requestArray = {
                "curentPage": 'all'
            };
            this.$emit('changepage',requestArray);
        },
        addingGoods (val) {
            if (this.pagins[val] =='...') {
                return false;
            }
            //this.numberPagActive = val + 1;
            let requestArray = {
                "curentPage": this.pagins[val]
            };
            this.$emit('changepage',requestArray);
        },
    },
    data() {
        return {
            pageCount: 0,
            pagin: 1,
            pagins: [],//страница

        }
    },
    props:['countElement','countElementScreen','curentPage']//Кол-во страниц,кол-во элементов, текущая страница
}