<?php

namespace App\Http\Controllers\Api\V1;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Classes\OperationFile;
use App\Classes\OperationUser;

class ApiController extends Controller
{

    public function index()
    {


        $out['books'] = Book::with('files','files.user')->get();

        return  json_encode($out, JSON_HEX_TAG | JSON_HEX_APOS);

    }

    public function store(Request $request)//Создаем запись
    {
        $this->validate($request, [
            'name' => 'bail|required|min:3',
            'subject' => 'bail|required|min:10',
        ]);

        $data = $request->all(); //забираем все данные
        $object = [
            'name' => $data['name'],
            'subject' => $data['subject']
        ];

       $booksId = Book::create($object)->id;
        $result = empty($booksId) ? false : true;
        $operationUser = new OperationUser;
        $user = ($data['userToken'] != null) ? $operationUser->checkAuthorise($data['userToken']) : '';

        if (!empty($user['id'])) {
            $object['user_id'] = $user['id'];
            $operationFile = new OperationFile;
            $result =  $operationFile->createFile($user['id'], $booksId, $data);
        }

        return json_encode($result, JSON_HEX_TAG | JSON_HEX_APOS);
    }


}
