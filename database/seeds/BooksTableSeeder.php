<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class, 20)->create()->
        each(
            function ($user) {
                factory(App\Book::class, 25)->create()->each(function ($book) use ($user) {
                    $book->files()->save(factory(App\File::class)->create(['user_id' => $user->id, 'book_id' => $book->id]));
                }
                );
            }
        );
    }

}
