export default {
    data: function () {
        return {
            companies: [],
            subject: '',
            file: '',
            name: '',
            userToken: '',
            error: false
        }
    },
    mounted() {
        this.name = $('#user-name').attr('data-user-login');
        this.userToken = $('#user-token').attr('data-user-token');
    },
    methods: {

        handleFileUpload(){
            this.file = this.$refs.file.files[0];
        },
        save() {
            let formData = new FormData();
            let file = this.file;
            formData.append('file', file);
            formData.append('name', this.name);
            formData.append('subject', this.subject);
            formData.append('userToken', (typeof this.userToken == 'undefined') ? '' : this.userToken);
            console.log(formData);
            axios.post('/api/v1/books', formData,
                {
                    headers: {
                         'Content-Type': 'multipart/form-data'
                    }
                })
                .then((response) => {
                    console.log(response);
                    if (response['data'] == true) {
                        this.$router.push({path: '/'});
                    }

                })
                .catch((error) => {
                    this.error = true;
                    console.log(error);
                });


        }
    }
}
