require('./bootstrap');

window.Vue = require('vue');

import VueRouter from 'vue-router';
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

window.Vue.use(VueRouter);

import BooksIndex from './components/views/booksIndex/BooksIndex.vue';
import BooksCreate from './components/views/booksCreate/BooksCreate.vue';



const routes = [
    {
        path: '/',
        components: {
            booksIndex: BooksIndex
        }
    },
    {path: '/admin/books/create', component: BooksCreate, name: 'createBooks'},

]

const router = new VueRouter({ routes })

const app = new Vue({ router }).$mount('#app')
